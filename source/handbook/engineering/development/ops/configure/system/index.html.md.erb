---
layout: handbook-page-toc
title: Configure:System Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of where this team is going take a look at [the product
vision](/direction/configure/).

As a member of the Ops Section you may also like to understand [our
overall vision](/direction/ops/).

## Mission

The Configure System group is responsible for developing Ops focused features of GitLab
that relate to the "Configuration" and "Operations" stages of the DevOps
lifecycle. These refer to the configuration of Serverless functions and applications deployed via GitLab as well as Infrastructure as Code (IaC), ChatOps, and Runbooks.

Major efforts thus far have focused on empowering developers to build Serverless applications with Knative.

As per the [product categories](/handbook/product/categories/) this team
is responsible for building out new feature sets that will allow
GitLab users to easily make use of the following modern DevOps practices:

- Serverless
- Infrastructure as Code (IaC)
- Runbooks
- ChatOps

We work collaboratively and transparently and we will contribute as much of our
work as possible back to the open source community.

## Team Members

### Backend Team

<%= direct_team(manager_role: 'Engineering Manager, Configure:System') %>

### Frontend Team

<%= direct_team(manager_role: 'Frontend Engineering Manager, Configure') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Configure/, direct_manager_role: 'Engineering Manager, Configure:System', other_manager_roles: ['Frontend Engineering Manager, Configure', 'Engineering Manager, Configure:System']) %>

## Common Links

 * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1242752?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asystem)
 * [Slack Team Channel](https://gitlab.slack.com/archives/g_configure_system)
 * [Slack Standup Channel](https://gitlab.slack.com/archives/s_configure_standup)
 * [Team Metrics](https://app.periscopedata.com/app/gitlab/547821/Configure:System-Team-Metrics)
 * [Weekly Meeting Agenda](https://docs.google.com/document/d/1liyzjjTfzyGMZ2Mcnw7v74xb5MKY2_g-lbBamQssGn4/edit#heading=h.du4aclq33fqa)

## Processes

### Planning
We use planning issues to work out the planning for the next milestone asynchronously.

1. The PM opens up a new milestone planning issue at the start of every milestone and lists the issues that we should be working on in priority order.
1. All stakeholders (PM, UX, Engineering) are invited to collaborate on the planning issue for the next milestone.
1. A sync meeting is held to discuss and debate the planning issue. All stakeholders collaborate on solution validation.
1. For any issue that is not fully groomed, the PM assigns those issues to the EM for further clarification or grooming.
1. The EM may work with Engineers to groom and validate these issues.
1. The EM reviews the issues to determine whether there will be sufficient capacity to deal with all the issues listed.
1. EM also add bugs to the milestone that should be worked on based on Priority and Severity labels.
1. The EM assigns `workflow::ready for development` to issues that are fully groomed.

[Example planning issue](https://gitlab.com/gitlab-org/configure/general/issues/7)

### Scheduling

#### Feature development
Our goal is to move towards a continuous delivery model such that the team completes tasks regularly and keeps working off of a prioritized backlog of issues and as such we default to team members self-scheduling their work:

* The team has weekly meetings to discuss issue priorities and to do issue grooming.
* For any issue that is not fully groomed, the PM assigns those issues to the EM for further clarification or grooming.
* The EM assigns `workflow::ready for development` to issues that are fully groomed.
* Team members self-assign issues from the [Configure:System issue board](https://gitlab.com/groups/gitlab-org/-/boards/1242752?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asystem) that are in the `workflow:ready for development` column.
* Once a team member has completed their assigned issues, they are expected to go to the group issue boards and assign themselves to the next unassigned `workflow:ready for development` issue.
* The issues on the board are in priority order based on importance (the higher they are on the list, the higher the priority). This order is set by the product manager.
* If all issues are assigned for the milestone, team members are expected to identifying the next available issue to work on based on the team's work prioritization (see below).
* While backstage work is important, in the absence of specific prioritization the team will have a bias towards working on `bug` or `feature` categorized issues.

#### Bug fixing and priortized work
In addition to the self-scheduling of feature development, the manager will from time-to-time assign bugs, or other work deemed important, directly to a team member.

### MR reviews

Team members should use their best judgment to determine whether to assign the first review of an MR based on the DangerBot's suggestion or to someone else on the team. Some factors in making this decision may be:

* If there is known domain expert for the area of code, prefer assigning the initial review to them.
* Does the MR require a lot of context to understand what it is doing? Will it take a reviewer outside the team a long time to ramp up on that context?
* Does the MR require a lot of dev environment setup that team members outside the Configure stage are likely to not have?
* Is the MR part of a larger effort for which a specific team member already has all the context?

### Backend & Frontend Issue Collaboration

Our team follows the GitLab [workflow guidelines for working in teams](/handbook/engineering/workflow/#working-in-teams).

Given a reasonable sized issue, that requires UX, frontend and backend work, the preferred way to collaborate on the issue is as follow:
1. Once an issue is labeled `workflow::ready for development`, backend is usually able to start working on the issue.
  1. This is a good time to discuss and clarify interdependency between backend and frontend
1. Backend and frontend will work on the issue in separated branches
  1. Each will submit their own MR for review
  1. Frontend and backend will put the feature behind a feature flag
1. Once both MRs have been merged, a third MR will be opened to integrate the work where backend and frontend will collaborate to:
  1. Remove the feature flag
  1. Add documentation
  1. Implement e2e tests for the feature

The above is a guideline and clear communication should be preferred over process to ensure the best collaboration strategy on an issue. For example on smaller issues, or where the frontend component of the work is minor, it may be feasible to work on the same branch.
